{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.11";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
      };
    in {
      devShell = pkgs.mkShell {
        inputsFrom = [
          self.packages.${system}.ltl2ba
        ];
      };

      packages = {
        ltl2ba = pkgs.stdenv.mkDerivation {
          name = "ltl2ba";
          src = ./.;
          nativeBuildInputs = [ pkgs.autoreconfHook ];
        };
      };
    });
}
